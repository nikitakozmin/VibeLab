const MovieDB = {
    movies: [],
};

const frm = document.querySelector("form");
const like = frm.querySelector("[name='like']");
const input = frm.querySelector("[name='name']");
const moviesList = document.querySelector("#movies-list");

let updatePageList = function(list) {
    moviesList.innerHTML = "";
    for (let i = 0; i < list.length; i++) {
        const text = document.createElement("p");
        text.setAttribute("data-index", i);
        text.innerHTML = list[i] + "&nbsp;&nbsp;";
        const bucket = document.createElement("button");
        bucket.textContent = "🗑️";
        if (!bucket.onclick) {            
            bucket.onclick = function(e) {
                list.splice(e.target.parentElement.getAttribute("data-index"), 1);
                updatePageList(list);
            };
        }
        text.appendChild(bucket);
        moviesList.appendChild(text);
        input.value = "";
    }
}

frm.addEventListener("submit", (e) => {
    e.preventDefault();
    // const formData = new FormData(frm);
    // MovieDB.movies.push(formData.get("name"));
    if (like.checked) {
        console.log('Добавляем любимый фильм');
        like.checked = false;
    }
    if (input.value.length > 21) {
        input.value = `${input.value.slice(0, 22)}...`;
    }
    MovieDB.movies.push(input.value);
    MovieDB.movies.sort();
    updatePageList(MovieDB.movies);
});
