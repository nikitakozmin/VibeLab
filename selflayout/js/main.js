$(document).ready(function(){


    var titles = ["Покупка авто", "Доставка морем", "Растаможка авто", "Ремонт авто", "Сертификация", "Постановка на учет"];
    var text = ["Подбор автомобиля и экспертная проверка",
                "Расчет оптимальной стоимости доставки авто",
                "Прохождение таможенного оформления (2-3 дня)",
                "Комплексный ремонт автомобиля на  СТО",
                "Услуга предоставляется по желанию",
                "Оформление автомобиля в Украине"]
    $(".service-slider").slick({
        vertical: true,
        verticalSwiping: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: true,
        customPaging : function(slider, i) {
            return '<button>' + '<h2 class="service-number">' + 0 + (i+1) + '</h2>' + '<h3>' + titles[i] + '</h3>' + '<h4>' + text[i] + '</h4>' + '</button>';
        }
    });
    
    $(".car-slider").slick({
        swipe: false,
        autoplaySpeed: 2000,
        infinity: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: true,
        dots: true,
    });


});