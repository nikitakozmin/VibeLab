let numberOfFilms;
do {
    numberOfFilms = prompt("Сколько фильмов вы уже посмотрели?");
} while (numberOfFilms == "" || numberOfFilms == null || numberOfFilms.length > 50);

const personalMovieDB = {
    count: +numberOfFilms,
    movies: {},
    actors: {},
    genres: [],
    privat: false,
};


function showMyDB() {
    if (!personalMovieDB.privat) {
        console.log(personalMovieDB);
    }
}

function writeYourGenres() {
    for (let i = 0; i < 3; i++) {
        personalMovieDB.genres[i] = prompt(`Ваш любимый жанр под номером ${i+1}`);
    }
}


if (personalMovieDB.count < 10) {
    alert("Просмотрено довольно мало фильмов");
} else if (10 <= personalMovieDB.count && personalMovieDB.count <= 30) {
    alert("Вы классический зритель");
} else if (personalMovieDB.count > 30) {
    alert("Вы киноман");
} else {
    alert("Произошла ошибка");
}

let key = prompt("Один из последних просмотренных фильмов?");
while (key == "" || key == null || key.length > 50){
    key = prompt("Один из последних просмотренных фильмов?");
}

let value;
for (value = prompt("На сколько оцените его?"); value == "" || value == null || value.length > 50; value = prompt("На сколько оцените его?"));

personalMovieDB.movies.key = value;
writeYourGenres();
showMyDB();
