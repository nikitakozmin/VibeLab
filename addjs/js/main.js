let serviceSlider = document.querySelector(".service-slider");
let serviceSliderItems = document.querySelectorAll(".service-slider__item");

showActive = function(items) {
    for (let i = 0; i < items.length; i++) {
        items[i].style.display = "none";
        items[i].classList.forEach(element => {        
            if (element == "active") {
                items[i].style.display = "block";
            }
        });
    }
}

showActive(serviceSliderItems);

let titles = ["Покупка авто", "Доставка морем", "Растаможка авто", "Ремонт авто", "Сертификация", "Постановка на учет"];
let text = ["Подбор автомобиля и экспертная проверка",
            "Расчет оптимальной стоимости доставки авто",
            "Прохождение таможенного оформления (2-3 дня)",
            "Комплексный ремонт автомобиля на  СТО",
            "Услуга предоставляется по желанию",
            "Оформление автомобиля в Украине"]
let serviceButtons = document.querySelector(".service-slider__buttons");

// Add buttons
serviceButtons.innerHTML += '<button class="active">' + '<h2 class="service-number">' + 0 + 1 + '</h2>' + '<h3>' + titles[0] + '</h3>' + '<h4>' + text[0] + '</h4>' + '</button>';
for (let i = 1; i < titles.length; i++) {
    serviceButtons.innerHTML += '<button>' + '<h2 class="service-number">' + 0 + (i+1) + '</h2>' + '<h3>' + titles[i] + '</h3>' + '<h4>' + text[i] + '</h4>' + '</button>';
}

let serviceSliderButtons = document.querySelectorAll(".service-slider__buttons>button");

serviceButtons.addEventListener("click", (e) => {
    if (e.target.parentElement.tagName == "BUTTON") {
        serviceSliderButtons.forEach(element => {        
            element.classList.remove("active");
        });
        let i = e.target.parentElement.innerText[1] - 1;
        serviceSliderButtons[i].classList.add("active");
        serviceSliderItems.forEach(element => {        
            element.classList.remove("active");
        });
        serviceSliderItems[i].classList.add("active");
        showActive(serviceSliderItems);
    }
    else if (e.target.tagName == "BUTTON") {
        let i = e.target.innerText[1] - 1;
        serviceSliderButtons[i].classList.add("active");
        serviceSliderItems.forEach(element => {
            element.classList.remove("active");
        });
        serviceSliderItems[i].classList.add("active");
        showActive(serviceSliderItems);
    }
});

let modalWindow = document.querySelector(".modal-window");

let consultation = document.querySelectorAll(".consultation");
consultation.forEach(element => {
    element.addEventListener("click", (e) => {
        modalWindow.style.display = "flex";
        document.body.style.overflow = "hidden";
        for (let i = 1; i < document.body.children.length; i++) {
            document.body.children[i].style.opacity = 0.5;
        }
    });
});

modalWindow.children[1].addEventListener("click", (e) => {
    modalWindow.style.display = "none";
    document.body.style.overflow = "visible";
    for (let i = 1; i < document.body.children.length; i++) {
        document.body.children[i].style.opacity = 1;
    }
});


$(document).ready(function(){


    // var titles = ["Покупка авто", "Доставка морем", "Растаможка авто", "Ремонт авто", "Сертификация", "Постановка на учет"];
    // var text = ["Подбор автомобиля и экспертная проверка",
    //             "Расчет оптимальной стоимости доставки авто",
    //             "Прохождение таможенного оформления (2-3 дня)",
    //             "Комплексный ремонт автомобиля на  СТО",
    //             "Услуга предоставляется по желанию",
    //             "Оформление автомобиля в Украине"]
    // $(".service-slider").slick({
    //     vertical: true,
    //     verticalSwiping: true,
    //     slidesToShow: 1,
    //     slidesToScroll: 1,
    //     arrows: false,
    //     dots: true,
    //     swipe: false,
    //     customPaging : function(slider, i) {
    //         return '<button>' + '<h2 class="service-number">' + 0 + (i+1) + '</h2>' + '<h3>' + titles[i] + '</h3>' + '<h4>' + text[i] + '</h4>' + '</button>';
    //     }
    // });
    
    $(".car-slider").slick({
        swipe: false,
        autoplaySpeed: 2000,
        infinity: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: true,
        dots: true,
    });


});
